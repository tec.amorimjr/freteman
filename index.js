/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/app';
import LogLocation from './src/LogLocation'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);

//AppRegistry.registerHeadlessTask('LogLocation', () => LogLocation);
