import React, { Component } from 'react';
import { Container, Button, Header, Content, Card, CardItem, Text, Body, Form, Label, Item, Input } from 'native-base'
import {ImageBackground, Image, Alert, Linking} from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import OneSignal from 'react-native-onesignal';
import axios from 'axios'
import api from './services/api'
import AsyncStorage from '@react-native-community/async-storage';
import base64 from 'react-native-base64'
import NetInfo from '@react-native-community/netinfo'

export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
          enviarcod: false,
          celular: null,
          smscod: null,
          sms_valida: null,
          codsms: '1',
          tocelular: null,
          id_motorista: null,
          display_name: null,
          error: '',
          isConnected: true
        }
        //OneSignal.inFocusDisplaying (2)
        OneSignal.init("2d16a97f-880a-4f9d-8c44-89ad4a693914")

        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
        //OneSignal.configure(); 
    }

    componentWillUnmount() {
     
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened);
      OneSignal.removeEventListener('ids', this.onIds);
      NetInfo.isConnected.removeEventListener('ConnectionChange', this.handleConnectivityChange)
      
    }
    componentDidMount () {
      AsyncStorage.getItem("id_motorista").then((value) => {
        if (value) { 
          OneSignal.sendTags({user: 'motorista', version: "1.1", id: value})
          this.props.navigation.navigate('App') }
      })
      NetInfo.isConnected.addEventListener('ConnectionChange', this.handleConnectivityChange)
    }

    handleConnectivityChange = isConnected => {
      if (isConnected) {
        this.setState({isConnected})
      } else {
        this.setState({isConnected})
      }
    }
    onReceived(notification) {
      console.log("Notification received: ", notification);
      AsyncStorage.setItem("updatePush", "OK")
    }
  
    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
    }
  
    onIds(device) {
      console.log('Device info: ', device);
    }

    validaSms = () => {
      console.log('sms_valida: ', this.state.sms_valida)
      if (this.state.codsms.toString() === this.state.sms_valida) {
        AsyncStorage.setItem("id_motorista", this.state.id_motorista)
        AsyncStorage.setItem("display_name", this.state.display_name)
        OneSignal.sendTags({user: 'motorista', version: "1.1", id: this.state.id_motorista})
        this.props.navigation.navigate('App')
      } else {
        Alert.alert(
          'Código',
          'O código digitado não confere.',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      }
    }
    
    login = () => {

      api.get('/login-motorista/'+this.state.tocelular)
      .then(response => {
        if (response.data[0].hasOwnProperty('tipo')) {
          this.setState({id_motorista: response.data[0].user_id, display_name: response.data[0].display_name})
          this.loadersms()
        } else {
          
        }
        
        })
      .catch( erro => {
        console.log('Voce precisa se cadastrar como motorista')
        Alert.alert(
          'Error',
          'Seu cadastro não é de Motorista.',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
        this.setState({enviarcod: !this.state.enviarcod})
})
}

    enviarsms = () => {
      this.login()
      //this.loadersms()
      
        this.setState({enviarcod: !this.state.enviarcod, celular: null, smscod: null})
    }

    loadersms = () => {
      var numero_aleatorio = Math.random();

      numero_aleatorio = Math.floor(numero_aleatorio * 10000);

      this.setState({codsms: numero_aleatorio}, ()=>{
        console.log('codsms: ', this.state.codsms)
      })
      var celular = '+55'+this.state.tocelular
      var textosms = 'Seu código Freteman é ' + numero_aleatorio
      var formBody = new FormData()
      formBody.append('Body',textosms)
      formBody.append('To',celular)
      formBody.append('From','+18339884211')

      const tok = 'AC7a04fa0a2715bc48a0d54982da4a0282:4aadeead6319c5c9d5b8b1f3baa934a5';
        const hash = base64.encode(tok);
        const Basic = 'Basic ' + hash;

      axios.post('https://api.twilio.com/2010-04-01/Accounts/AC7a04fa0a2715bc48a0d54982da4a0282/Messages', 
      formBody, 
      {
        // auth: {
        //   username: base64.encode('AC7a04fa0a2715bc48a0d54982da4a0282'),
        //   password: base64.encode('4aadeead6319c5c9d5b8b1f3baa934a5')
        // },
      headers: {'Content-Type': 'multipart/form-data', 'Authorization' : Basic}
      }
        
        )
    .then(res => {
      console.log(res)
      // if (res.data.success === true) {
      //   this.setState({smsativo: true})
      // }
      //   this.setState({agenda: res.data}, ()=> {
      //     this.props.navigation.navigate('Agendamentos')
      //   })
    })
    .catch(err => {
        console.log(err)
  
        Alert.alert(
          'Error',
          err.toString(),
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
    })

      
    }

  render() {
    if (!this.state.isConnected){
      this.props.navigation.navigate('Off')
    }
    return (
      <Container>
        
        <Grid>
          <Row style={{ backgroundColor: '#FFF', height: '35%' }}>
          <ImageBackground source={require('../image/Background.png')} style={{width: '100%', height: '100%', justifyContent: 'center',
                alignItems: 'center'}}>
            <Image source={require('../image/logo.png')}/>
            </ImageBackground>
          </Row>
          <Row style={{ backgroundColor: '#FFF', height: '40%' }}>
          <Content style={{marginTop: -60, width: '80%' }} padder borderRadius={5}>
          <Card style={{ borderRadius: 20 }}>
            <CardItem header style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, alignItems: 'center', justifyContent:'center' }}>
              <Text style={{fontFamily:'Nutito', fontSize: 18, color: '#333', textAlign:'center'}}>Receba um SMS para entrar</Text>
            </CardItem>
            <CardItem cardBody>  
            <Form style={{ width: '90%', marginBottom:30}}>
            {this.state.enviarcod ? 
            <Item floatingLabel>
            <Label>Insira o código</Label>
            <Input keyboardType = {'numeric'} onChangeText={(text) => this.setState({smscod:text, sms_valida:text})} value={this.state.smscod}/>
          </Item>
          :
          <Item floatingLabel>
              <Label>Celular</Label>
              <Input keyboardType = {'numeric'} onChangeText={(text) => this.setState({celular:text, tocelular:text})} value={this.state.celular} />
            </Item>
        }
          </Form>
                
        
            </CardItem>
            <CardItem footer style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 , height: 50, justifyContent:'center', alignItems: 'center'}}>
            <Button onPress={() => {
                if( this.state.celular){
                    this.enviarsms()} else { 
                      if (this.state.smscod ) {
                        this.validaSms()
                      } else {

                      

                        Alert.alert(
                            'Login',
                            'Preencha o campo para prosseguir',
                            [
                              {text: 'OK', onPress: () => console.log('OK Pressed')},
                            ],
                            { cancelable: false }
                          )

                    }}
                }} rounded style={{marginTop: 30, width: 230, height: 50, justifyContent:'center', alignItems: 'center', backgroundColor:'#0FC874'}}>
            {this.state.enviarcod ? <Text>Enviar Código</Text> : <Text>Receber SMS</Text>}
          </Button>
            </CardItem>
         </Card>
         </Content>
          </Row>
          <Row style={{ backgroundColor: '#FFF', height: '10%', justifyContent:'center'}}>
          <Image source={require('../image/or.png')}/>
          </Row>
          <Row style={{ backgroundColor: '#FFF', height: '15%', justifyContent:'center'}}>
          <Text button onPress={() => Linking.openURL('https://app.freteman.com.br/seja-um-freteman/').catch((err) => console.error('An error occurred', err))}>Crie uma conta. <Text style={{fontWeight: 'bold', color:'#0FC874'}}>Criar</Text></Text>
          
          <Text>{this.state.error}</Text></Row>
          
        </Grid>
        
      </Container>
    );
  }
}