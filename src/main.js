import React, { Component } from 'react';
import { Container, Header, Content, Tab, Tabs, Drawer } from 'native-base';
import {TouchableOpacity, Alert} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import SideBar from './sideBar'
import Tab1 from './ungoing';
import Tab2 from './scheduled';
import Tab3 from './completed';
import LogoHeader from './component/logoheader'
import Geolocation from '@react-native-community/geolocation';
import api from './services/api'
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo'

export default class Main extends Component {

  static navigationOptions = ({navigation}) => ({
    headerTitle: <LogoHeader />,
    headerTintColor: '#FFF',
    headerStyle: {
      backgroundColor: '#E7413B'
    },
    headerRight: (
      <TouchableOpacity onPress={navigation.getParam('toggleDrawer')}  >
      
      <Icon name={navigation.getParam('iconMenu')} size={25} color='#FFF' style={{marginRight:10}} />
     
      </TouchableOpacity>
  )
  });

  state = {
    openMenu: false,
    icon: 'bars',
    latitude: null,
    longitude: null,
    motorista: null,
    isConnected: true
  }
  findCoordinates = () => {
    Geolocation.getCurrentPosition(
      position => {
        const latitude = JSON.stringify(position.coords.latitude);
        const longitude = JSON.stringify(position.coords.longitude);


        this.setState({ latitude, longitude }, ()=>{
          console.log('latitude: ',this.state.latitude)
          console.log('longitude: ',this.state.longitude)
          this.saveCoordenadas()

        });
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: false, timeout: 2000, maximumAge: 3600000 }
    );
  }
  saveCoordenadas = () => {

      var formBody = {
        "latitude":this.state.latitude,
        "longitude":this.state.longitude,
        "motorista":this.state.motorista,
        "time": new Date()
      }

      //const tok = 'AC7a04fa0a2715bc48a0d54982da4a0282:4aadeead6319c5c9d5b8b1f3baa934a5';
       // const hash = base64.encode(tok);
       // const Basic = 'Basic ' + hash;

      api.post('/motoristas-coord', 
      formBody, 
      {
        // auth: {
        //   username: base64.encode('AC7a04fa0a2715bc48a0d54982da4a0282'),
        //   password: base64.encode('4aadeead6319c5c9d5b8b1f3baa934a5')
        // },
      headers: {'Content-Type': 'application/json'}
      }
        
        )
    .then(res => {
      console.log(res)
      // if (res.data.success === true) {
      //   this.setState({smsativo: true})
      // }
      //   this.setState({agenda: res.data}, ()=> {
      //     this.props.navigation.navigate('Agendamentos')
      //   })
    })
    .catch(err => {
        console.log(err)
  
        Alert.alert(
          'Error',
          err.toString(),
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
    })

  }
  componentDidMount() {
    AsyncStorage.getItem("id_motorista").then((value) => {
      this.setState({motorista: value})
    })
    this.findCoordinates()
    this.props.navigation.setParams({ toggleDrawer: this.openDrawer });
    this.props.navigation.setParams({ iconMenu: this.state.icon });
    this.interval = setInterval(this.findCoordinates, 60000);
    //this.interval = setInterval(this.saveCoordenadas, 90000);
    NetInfo.isConnected.addEventListener('ConnectionChange', this.handleConnectivityChange)
  }

  handleConnectivityChange = isConnected => {
    if (isConnected) {
      this.setState({isConnected})
    } else {
      this.setState({isConnected})
    }
  }

  componentWillMount () {
    NetInfo.isConnected.removeEventListener('ConnectionChange', this.handleConnectivityChange)
  }

  closeDrawer = () => {
    this.setState({openMenu: false, icon: 'bars'}, () => {
      this.props.navigation.setParams({ iconMenu: this.state.icon });
    })
  }
  openDrawer = () => {
    if (this.state.openMenu) {
      this.drawer._root.close()
      this.setState({openMenu: false, icon: 'bars'}, () => {
        this.props.navigation.setParams({ iconMenu: this.state.icon });
      })
    } else {
      this.drawer._root.open()
      this.setState({openMenu: true, icon: 'times'}, () => {
        this.props.navigation.setParams({ iconMenu: this.state.icon });
      })
      

    }
    
  }
  _iconMenu = () => {
  if (!this.state.openMenu) {
    this.setState({icon})
  } 
  }

  render() {
    if (!this.state.isConnected){
      this.props.navigation.navigate('Off')
    }
    return (
      <Drawer ref={(ref) => { this.drawer = ref; }} 
        content={<SideBar  navigator={this.navigator} />} 
        onClose={() => this.closeDrawer()} 
        style={{backgroundColor:'#FFF'}}> 
        
      <Container>
        {/* <Header hasTabs /> */}
        
        <Tabs onChangeTab={({ i, ref, from })=> console.log(i)} >
          <Tab navigation={this.props.navigation} heading="Andamento" tabStyle={{backgroundColor: '#E7413B'}} textStyle={{color: '#fff', fontFamily: 'Nunito'}} activeTabStyle={{backgroundColor: '#E7413B'}} activeTextStyle={{color: '#fff', fontWeight: 'normal', fontFamily: 'Nunito'}}>
            <Tab1 />
          </Tab>
          
          <Tab heading="Agendados" tabStyle={{backgroundColor: '#E7413B'}} textStyle={{color: '#fff', fontFamily: 'Nunito'}} activeTabStyle={{backgroundColor: '#E7413B'}} activeTextStyle={{color: '#fff', fontWeight: 'normal', fontFamily: 'Nunito'}}>
            <Tab2 />
          </Tab>
          <Tab heading="Histórico" tabStyle={{backgroundColor: '#E7413B'}} textStyle={{color: '#fff', fontFamily: 'Nunito'}} activeTabStyle={{backgroundColor: '#E7413B'}} activeTextStyle={{color: '#fff', fontWeight: 'normal', fontFamily: 'Nunito'}}>
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
      </Drawer>
    );
  }
}