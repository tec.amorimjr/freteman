import React, { Component, PureComponent } from 'react';
import {TouchableOpacity, FlatList, StyleSheet, View} from 'react-native'
import { Container, Header, Content, Tab, Tabs, Text, Card, CardItem, Body } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Logo$ from './component/image$'
import LogoLocaliza from './component/logolocaliza'
import { withNavigation } from 'react-navigation';
import api from './services/api'
import Moment from 'moment'
import OneSignal from 'react-native-onesignal'
import { Divider } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';
import { SwipeListView } from 'react-native-swipe-list-view';

//import console = require('console');

class Ungoing extends Component {

  constructor(props) {
    super(props);
    this.state = {
    pedidos: [],
    pedidos2: [],
    docs: [],
    spinner: false,
    updatePush: 'NO',
    motorista: null,
    refresh: false,
    refreshing: false
}

  }

  componentDidMount() {
  AsyncStorage.getItem("id_motorista").then((value) => {
  this.setState({ spinner: true, motorista: value}, () => {
   
      this.loaderPedidos()
    
  })
  this.interval = setInterval(this.loaderPedidos, 10000);
})
  }
  updatePedidos = () => {
    AsyncStorage.getItem("updatePush").then((value) => {
        this.setState({updatePush: value}, () => {
            if (this.state.updatePush === 'OK') {
                this.setState({refresh:true})
                console.log('Carregou pelo push mode')
                this.loaderPedidos()
                this.setState({ refresh:false})
                AsyncStorage.setItem("updatePush", "NO")
                AsyncStorage.setItem("updatePush2", "OK")
            }
        })
    })
}
handleRefresh = () => {
  this.setState({
      refreshing: true,
  })
  this.loaderPedidos()
}

  loaderPedidos = () => {

        api.get('/get-pedidos/'+this.state.motorista)
        .then(response => {
          this.setState({pedidos: response.data, spinner: false,  refreshing: false}, () => {
            //AsyncStorage.setItem("pedidos", this.state.pedidos)
            
            console.log(this.state.pedidos)
          })
        
}).catch( erro => {
  this.setState({spinner: false,  refreshing: false})
})
  }

  

  abrirOrder = () => {
    this.props.navigation.navigate('Order')
  }

  renderItem = ({ item }) => {  
    const sServico = (item.status === 'aguardando' || item.status === 'em-analise' ? 'novo' : item.status === 'confirmado' ? 'agendado' : item.status === 'em-execucao' ? 'iniciado' : item.status)
    const corServico = (item.status === 'aguardando' || item.status === 'em-analise' ? '#FFC90A' : item.status === 'cancelado' ? '#EF1921' : item.status === 'concluido' ? '#c2c2c2' : item.status === 'confirmado' ? '#23B04A' : item.status === 'em-execucao' ? '#2146B0' : '#000')
    const sDisplay = (item.status === 'aguardando' || item.status === 'em-execucao' || item.status === 'em-analise' ? 'flex' : 'none' )

    if (item.status === 'aguardando' || item.status === 'em-execucao' || item.status === 'em-analise') {
      return (
          <TouchableOpacity 
          onPress={() => this.props.navigation.navigate('Order', {pedido: item})}
          >
          <Card style={{ borderRadius: 10}}>
              <CardItem header style={{borderTopLeftRadius: 10, borderTopRightRadius: 10}}>
              <Grid>
                <Col size={60}>
                <Row>
                <Text style={{color:'#a2a2a2', fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}>{item.id_aleatoreo}</Text>
                <Text> - </Text>
                <Text style={{color:'#3D434A', fontWeight:'bold', fontSize:13, fontFamily: 'Nunito'}}>{item.servico.split(' ')[0]}</Text>
                </Row></Col>
                <Col style={{alignItems:'flex-end'}} size={40}>
                <Text style={{color:'#a2a2a2', fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}> {Moment(item.hora_motorista).format('DD/MM/YY - HH')}h</Text>
                </Col>
                </Grid>
              </CardItem>
              <CardItem style={{borderLeftColor:corServico, borderLeftWidth:5, borderRadius:0}}>
                <Body>
                  <Grid>
                    <Col>
                    <Text style={{color:corServico, fontWeight:'bold', fontFamily: 'Nunito', textTransform: 'capitalize', fontSize:15}}>{sServico}</Text>
                    </Col>
                    <Col style={{alignItems:'flex-end'}}>
                    <Text style={{fontWeight: 'bold', fontFamily: 'Nunito', fontSize:15}}><Logo$ /> R$ {item.valor_total}</Text>
                    </Col>
                  </Grid>
                </Body>
              </CardItem>
              <CardItem >
                <View style={{justifyContent:'center', width:'100%'}}>
                <Divider style={{ backgroundColor: '#ededed', width:'100%' }} />
                </View>
              
              </CardItem>
              <CardItem style={{ borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent:'flex-start'}}>
              
              <Grid style={{marginTop:-12}}>
                <Col size={10} style={{alignContent:'center', justifyContent:'center'}}><LogoLocaliza /></Col>
                <Col size={90}><Text numberOfLines={2} style={{fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}>{item.local_partida}</Text></Col>
              </Grid>
                
              </CardItem>
           </Card>
           </TouchableOpacity>
      )
    } else {
      return (
        <Grid style={{marginTop: 50}}>
                      <Col style={{alignItems:'center'}}>
                      <Text style={{color:'#a2a2a2', fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}>Tudo tranquilo por aqui!</Text>
                      <Text style={{color:'#a2a2a2', fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}>Em breve teremos novidades.</Text>
                      </Col>
                      </Grid>
                    
      )
          }
      
        
      
    
    
  }

  render() {
    
    return (
      <Container>
        <Spinner
          visible={this.state.spinner}
          textContent={'Carregando...'}
          textStyle={styles.spinnerTextStyle}
        />
        <FlatList
                contentContainerStyle={styles.list}
                data={this.state.pedidos}
                keyExtractor={(item, index) => item.id_aleatoreo}
                renderItem={this.renderItem}
                initialNumToRender={10}
                //initialScrollIndex={-1}
                //extraData={this.state.pedidos}
                //ListHeaderComponent={this.renderHeader}
                refreshing={this.state.refreshing}
                onRefresh={this.handleRefresh}
                />
                {this.state.pedidos.length <= 0 && this.state.spinner == false ? 
                <Grid style={{marginTop: -50}}>
                  <Col style={{alignItems:'center'}}>
                <Text style={{color:'#a2a2a2', fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}>Tudo tranquilo por aqui!</Text>
                <Text style={{color:'#a2a2a2', fontWeight:'100', fontSize:13, fontFamily: 'Nunito'}}>Em breve teremos novidades.</Text>
                </Col>
                </Grid>
                : null}
                
                
                
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#fafafa"
  },
  spinnerTextStyle: {
    color: '#FFF',
    fontFamily: 'Nunito',
    fontSize: 13
  },
  list: {
      padding: 20
  },
  activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80
  },
  textWithoutItemns: {
      textAlign: 'center'
  },
  ocorrenciaButton: {
      backgroundColor: "#FEFE9A",
      borderWidth: 1,
      borderColor: "#FFFF00",
      borderRadius: 5,
      padding: 20,
      marginBottom: 20
  },
  ocorrenciaButtonRed: {
      backgroundColor: "#FF9C9C",
      borderWidth: 1,
      borderColor: "#FF0000",
      borderRadius: 5,
      padding: 20,
      marginBottom: 20
  },
  ocorrenciaButtonBlue: {
      backgroundColor: "#AAAAFD",
      borderWidth: 1,
      borderColor: "#0000FF",
      borderRadius: 5,
      padding: 20,
      marginBottom: 20
  },
  ocorrenciaTitle: {
      fontSize: 14,
      fontWeight: "bold",
      color: "#333"
  },
  ocorrenciaDescription: {
      fontSize: 16,
      color: "#333",
      marginTop: 5,
      lineHeight: 24
  },
  image: {
      borderRadius: 20,
      width: 35,
      height: 35,
      borderWidth: 1,
      borderColor: '#FFF',
      marginRight: 10
       //borderRadius: 200
   }
});

export default withNavigation(Ungoing);