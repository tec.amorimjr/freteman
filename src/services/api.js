import axios from 'axios';

const api = axios.create({
    baseURL: 'https://app.freteman.com.br/wp-json/api/v1',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
});

export default api;