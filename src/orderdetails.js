import React, { Component, Fragment } from 'react';
import { Container, Header, Content, Tab, Tabs, Text, Card, CardItem, Body, List, ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import IconCall from './component/iconcall'
import IconPickup from './component/iconpickup'
import IconDrop from './component/icondrop'
import {View, Alert, StyleSheet, Linking} from 'react-native'
import Moment from 'moment'
import IconMap from './component/icmap'
import api from './services/api'
import LineDrop from './component/line_drop'
import { Divider } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import AsyncStorage from '@react-native-community/async-storage';

export default class OrderDetails extends Component {
    static navigationOptions = () => ({
        headerTitle: 'Detalhes do pedido',
        headerTintColor: '#FFF',
        headerTitleStyle: {
          fontFamily:'Nunito',
          fontSize:16
        },
        headerStyle: {
          backgroundColor: '#E7413B'
        },
      });

      state = {
        rotas: [],
        spinner: false,
        origem: null
    };
    
      componentDidMount() {
        this.setState({spinner: true}, () => {
          this.loaderRotas()
        })
      
      }

      loaderRotas = () => {
        const { navigation } = this.props;

        api.get('/get-rotas/'+ navigation.state.params.pedido.ft_id)
        .then(response => {
          this.setState({rotas: response.data, spinner: false}, () => {
            console.log(this.state.rotas)
          })
        
}).catch( error => {
  this.setState({spinner: false})
})
  }

  actionButton = (value) => {
    Alert.alert(
      'Solicitação',
      'Você deseja realmente ' + value + ' o serviço?',
      [
        {text: 'Não', onPress: () => this.props.navigation.goBack()},
        {text: 'Sim', onPress: () => this.setState({spinner: true}, ()=>{this.loaderActions(value)})},
      ],
      { cancelable: false }
    )
  }
  loaderActions = (value) => {
    
    const { navigation } = this.props;

    api.get('/actions-motoristas-'+value+'/'+ navigation.state.params.pedido.ft_id)
    .then(response => {
      this.setState({spinner:false}, ()=>{
        AsyncStorage.setItem("updatePush", "OK")
        
        this.props.navigation.goBack()
      })
        
      
      
    })
    .catch(error => {
      this.setState({spinner:false})
    })
    
  }
  saveOrigem (value) {
    this.setState({origem: value})
  }
_maps = (value) => {
  Linking.openURL('geo:'+value+'?q='+value).catch((err) => console.error('An error occurred', err));
}
  render() {
    const { navigation } = this.props;

    const sayrotas = this.state.rotas.map((item) => {
      if (item.tipo !== 'origem') {
      return (
        <Fragment key={item.ID}>
        <Row style={{marginTop:10}}>

                  
<Col size={20} style={{alignItems:'center'}}>
<IconDrop />
</Col>
<Col size={80} style={{alignItems:'flex-start', justifyContent:'center'}}>
<View>
<Text  style={{fontWeight:'bold', fontSize:16, fontFamily: 'Nunito'}}>Ponto de entrega {item.ordem}</Text>
</View>

</Col>
</Row>
<Row>
    <Col size={20} style={{alignItems:'center', marginTop:10}}><LineDrop /></Col>
    <Col size={80}>
        <Row>
        <Col size={3}><Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.local} </Text></Col>
                    <Col size={1}><Text onPress={() => this._maps(item.local)}><IconMap  /></Text></Col>      
        </Row>
        <Row>
        <Col>
        <Row>
        <Text style={{fontSize:12, marginTop:10, fontFamily: 'Nunito'}}>Número</Text>
        </Row>
        <Row>
        <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.numero}</Text>
        </Row>
        <Row>
        <Text style={{fontSize:12, fontFamily: 'Nunito'}}>Contato nome</Text>
        </Row>
       <Row>
       <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.nome_contato}</Text>
       </Row>
        
        </Col>
        <Col>
        <Row>
        <Text style={{fontSize:12, marginTop:10, fontFamily: 'Nunito'}}>Complemento</Text>
        </Row>
        <Row>
        <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.complemento}</Text>
        </Row>
        <Row>
        <Text style={{fontSize:12, fontFamily: 'Nunito'}}>Telefone</Text>
        </Row>
        <Row>
        <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{navigation.state.params.pedido.status === 'aguardando' || navigation.state.params.pedido.status === 'em-analise' ? '(...)' : item.telefone}</Text>
        </Row>
        
        </Col>
      </Row>
      </Col>
      </Row>
      </Fragment>
      )
      } else {
        return null
      }
    })

    const saypcoleta = this.state.rotas.map((item) => {
      if (item.tipo === 'origem') {
      
        
        return (
          
          <Fragment key={item.ID}>
                  
                  <Row>
                    <Col size={3}><Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.local} </Text></Col>
                    <Col size={1}><Text onPress={() => this._maps(item.local)}><IconMap  /></Text></Col>      
                  </Row>
                  <Row>
                    <Col>
                    <Row>
                    <Text style={{fontSize:12, fontFamily: 'Nunito'}}>Número</Text>
                    </Row>
                    <Row>
                    <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.numero}</Text>
                    </Row>
                    <Row>
                    <Text style={{fontSize:12, fontFamily: 'Nunito'}}>Contato nome</Text>
                    </Row>
                   <Row>
                   <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.nome_contato}</Text>
                   </Row>
                    
                    </Col>
                    <Col>
                    <Row>
                    <Text style={{fontSize:12, fontFamily: 'Nunito'}}>Complemento</Text>
                    </Row>
                    <Row>
                    <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{item.complemento === '' ? '--:--' : item.complemento}</Text>
                    </Row>
                    <Row>
                    <Text style={{fontSize:12, fontFamily: 'Nunito'}}>Telefone</Text>
                    </Row>
                    <Row>
                    <Text style={{color:'#a2a2a2',fontSize:12, fontFamily: 'Nunito'}}>{navigation.state.params.pedido.status === 'aguardando' || navigation.state.params.pedido.status === 'em-analise' ? '(...)' : item.telefone}</Text>
                    </Row>
                    
                    </Col>
                  </Row>
                  </Fragment>
        )

      } else {
        return null
      }
      
    })
    
    return (
      <Container>
        <Spinner
          visible={this.state.spinner}
          textContent={'Carregando...'}
          textStyle={styles.spinnerTextStyle}
        />
          <Grid>
          <Row>
          <Content padder>
          <List>
            <ListItem itemDivider style={{backgroundColor:'white', borderLeftWidth:5, borderColor: '#0FC874'}}>
              <Text style={{color:'#0FC874', fontSize:17, fontFamily: 'Nunito', fontWeight:'bold'}}>Informações do pedido</Text>
            </ListItem>  
                              
            <ListItem noBorder>
              <Grid>
                <Row>
                  <Col>
                  <Row><Text style={{fontFamily: 'Nunito', fontSize:12}}>Pedido</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12}}>#{navigation.state.params.pedido.id_aleatoreo}</Text></Row>
                  <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Cliente</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12}}>{navigation.state.params.pedido.display_name}</Text></Row>
                  <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Serviço</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontSize:12, fontFamily: 'Nunito'}}>{navigation.state.params.pedido.servico.split(' ')[0]}</Text></Row>
                  <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Veículo</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12}}>{navigation.state.params.pedido.veiculo.split(' ')[0]}</Text></Row>
                  <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Valor total</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12}}>R$ {navigation.state.params.pedido.valor_total},00</Text></Row>
                  
                  </Col>
                  <Col>
                  <Row><Text style={{fontFamily: 'Nunito', fontSize:12}}>Data/Horário</Text></Row>
                  <Row><Text style={{color:'#a2a2a2',fontFamily: 'Nunito', fontSize:12}}>{Moment(navigation.state.params.pedido.hora_motorista).format('DD/MM/YY - HH')}h</Text></Row>
                  <Row><Text style={{marginTop:10,fontFamily: 'Nunito', fontSize:12}}>Celular</Text></Row>
                  <Row>{navigation.state.params.pedido.status === 'aguardando' || navigation.state.params.pedido.status === 'em-analise' ? <Text style={{color:'#a2a2a2',fontFamily: 'Nunito', fontSize:12}}>(...)</Text> : <Text style={{color:'#a2a2a2',fontFamily: 'Nunito', fontSize:12}} onPress={() => Linking.openURL('tel://0'+navigation.state.params.pedido.tel_cliente).catch((err) => console.error('An error occurred', err))}>{navigation.state.params.pedido.tel_cliente}</Text>}</Row>
                  <Row><Text style={{marginTop:10,fontFamily: 'Nunito', fontSize:12}}>Ajudantes</Text></Row>
                  <Row><Text style={{color:'#a2a2a2',fontFamily: 'Nunito', fontSize:12}}>{navigation.state.params.pedido.ajudantes}</Text></Row>
                  <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Distância</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12}}>{navigation.state.params.pedido.distancia_total.split('.')[0]}km, {navigation.state.params.pedido.distancia_total.split('.')[1]}m</Text></Row>
                  <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Seu ganho</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12}}>R$ {navigation.state.params.pedido.valor_motorista},00</Text></Row>
                  
                  </Col>
                </Row>
                <Row><Text style={{marginTop:10, fontFamily: 'Nunito', fontSize:12}}>Pagamento</Text></Row>
                  <Row><Text style={{color:'#a2a2a2', fontFamily: 'Nunito', fontSize:12, marginBottom:20}}>{navigation.state.params.pedido.metodo_pagamento}</Text></Row>
              </Grid>
            </ListItem>
           
            <ListItem itemDivider style={{backgroundColor:'white', borderLeftWidth:5, borderColor: '#0FC874'}}>
              <Text style={{color:'#0FC874', fontSize:17, fontFamily: 'Nunito', fontWeight:'bold'}}>Detalhes coleta e entrega</Text>
            </ListItem>  
            <ListItem itemDivider noBorder style={{backgroundColor:'white'}}>
              <Grid style={{marginLeft:-18}}>
                  
                  <Row>
                  <Col size={20} style={{alignItems:'center'}}>
                  <IconPickup />
                  </Col>
                  <Col size={80} style={{alignItems:'flex-start', justifyContent:'center'}}>
                  <View>
                  <Text style={{fontWeight:'bold', fontSize:16, textAlign: 'left', fontFamily: 'Nunito'}}>Ponto de coleta </Text>
                  </View>
                  
                  </Col>
                  </Row>
                  <Row >
                    <Col size={20} style={{alignItems:'center', marginTop:10}}><LineDrop /></Col>
                    <Col size={80}>{saypcoleta}</Col>
                  </Row>
                  
                  {sayrotas}
              </Grid>
            </ListItem>
          </List>
          </Content>
          </Row>
          {/* Go Action buttons */}
          {navigation.state.params.pedido.status === 'aguardando' || navigation.state.params.pedido.status === 'em-analise' ?
          <Row style={{height:80}}>
              <Col style={{height:80}} size={40}>
              <Content padder >
              <Card style={{ borderRadius: 27 }} transparent>
            <CardItem style={{backgroundColor:'#E7413B', borderRadius: 27}} button onPress={() => this.actionButton('recusar')}>
              <Body style={{justifyContent:'center', alignItems: 'center'}}>
                <Text style={{color:'#FFF', fontWeight:'bold', fontSize:15, fontFamily: 'Nunito'}}>
                   Recusar
                </Text>
              </Body>
            </CardItem>
          </Card>
          </Content>
          </Col>
              <Col style={{height:80}} size={60}>
              <Content padder>
              <Card style={{ borderRadius: 27 }} transparent>
            <CardItem style={{backgroundColor:'#0FC874', borderRadius: 27}} button onPress={() => this.actionButton('aceitar')}>
              <Body style={{justifyContent:'center', alignItems: 'center'}}>
                  <Text style={{color:'#FFF', fontWeight:'bold', fontSize:15, fontFamily: 'Nunito', textAlign:'center'}}>
                   Aceitar
                </Text>
              </Body>
            </CardItem>
          </Card>
          </Content>
          </Col>
          </Row>
          : navigation.state.params.pedido.status === 'em-execucao' ?
          <Row style={{height:80}}>
          <Col style={{height:80}}>
          <Content padder>
              <Card style={{ borderRadius: 27 }} transparent>
            <CardItem style={{backgroundColor:'#0FC874', borderRadius: 27}} button onPress={() => this.actionButton('finalizar')}>
              <Body style={{justifyContent:'center', alignItems: 'center'}}>
                  <Text style={{color:'#FFF', fontWeight:'bold', fontSize:15, fontFamily: 'Nunito', textAlign:'center'}}>
                   Finalizar Serviço
                </Text>
              </Body>
            </CardItem>
          </Card>
          </Content>
          </Col>
          </Row>
          : navigation.state.params.pedido.status === 'confirmado' ?
          <Row style={{height:80}}>
          <Col style={{height:80}}>
          <Content padder>
              <Card style={{ borderRadius: 27 }} transparent>
            <CardItem style={(Moment(navigation.state.params.pedido.hora_motorista).format() <= Moment().format())?{backgroundColor:'#0FC874', borderRadius: 27}:{backgroundColor:'#C2C2C2', borderRadius: 27}} button onPress={() => (Moment(navigation.state.params.pedido.hora_motorista).format() <= Moment().format())?this.actionButton('iniciar'):null}>
              <Body style={{justifyContent:'center', alignItems: 'center'}}>
                  <Text style={{color:'#FFF', fontWeight:'bold', fontSize:15, fontFamily: 'Nunito', textAlign:'center'}}>
                   Iniciar Serviço 
                </Text>
              </Body>
            </CardItem>
          </Card>
          </Content>
          </Col>
          </Row>
          : null }
          {/* Finish Actions Buttons */}
          </Grid>
          
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: "#fafafa"
  },
  spinnerTextStyle: {
    color: '#FFF',
    fontFamily: 'Nunito',
    fontSize: 13
  }
})