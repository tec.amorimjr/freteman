import React, { Component } from 'react';
import { Container, Button, Header, Content, Card, CardItem, Text, Body, Form, Label, Item, Input } from 'native-base'
import {ImageBackground, Image, Alert, Linking} from 'react-native'
import { Col, Row, Grid } from 'react-native-easy-grid';
import OneSignal from 'react-native-onesignal';
import axios from 'axios'
import api from './services/api'
import AsyncStorage from '@react-native-community/async-storage';
import base64 from 'react-native-base64'

export default class Offline extends Component {

    closed = () => {
        //AsyncStorage.clear()
        this.props.navigation.navigate('Auth')
      }

  render() {
    return (
      <Container>
        
        <Grid>
          <Row style={{ backgroundColor: '#FFF', height: '35%' }}>
          <ImageBackground source={require('../image/Background.png')} style={{width: '100%', height: '100%', justifyContent: 'center',
                alignItems: 'center'}}>
            <Image source={require('../image/logo.png')}/>
            </ImageBackground>
          </Row>
          <Row style={{ backgroundColor: '#FFF', height: '40%' }}>
          <Content style={{marginTop: -60, width: '80%' }} padder borderRadius={5}>
          <Card style={{ borderRadius: 20 }}>
            <CardItem header style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, alignItems: 'center', justifyContent:'center' }}>
              <Text style={{fontFamily:'Nutito', fontSize: 18, color: '#333', textAlign:'center'}}>Sem conexão com INTERNET!</Text>
            </CardItem>
            <CardItem cardBody style={{alignItems: 'center', justifyContent:'center'}}>  
           
                <Text style={{fontFamily:'Nunito', fontSize:14, textAlign:'center'}}>Não é possível utilizar o app sem conexão com a INTERNET.</Text>
        
            </CardItem>
            <CardItem footer style={{ borderBottomLeftRadius: 20, borderBottomRightRadius: 20 , height: 50, justifyContent:'center', alignItems: 'center'}}>
            
            </CardItem>
         </Card>
         </Content>
          </Row>
          <Row style={{ backgroundColor: '#FFF', height: '15%', justifyContent:'center'}}>
          <Text button onPress={() => this.closed()}>Sair do app. <Text style={{fontWeight: 'bold', color:'#0FC874'}}>Sair</Text></Text>
          
          </Row>
          
        </Grid>
        
      </Container>
    );
  }
}