import { createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator } from 'react-navigation';
import Main from './main'
import Login from './index'
import Order from './orderdetails'
import Ungoing from './ungoing'
import Offline from './offline'


const RotaFull = createStackNavigator({
    Main: {
        screen: Main
    },
    Order: {
      screen: Order
    }
})

const RotaOff = createStackNavigator({
  Offline: {
    screen: Offline
  },
},{
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
})

const AuthStack = createStackNavigator({ 
  SignIn: { screen: Login
  } 
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
 });

//const SingnupStack = createStackNavigator({ SignUp: Singup})

export default createAppContainer(createSwitchNavigator(
    {
      App: RotaFull,
      Auth: AuthStack,
      Off: RotaOff
    },
    {
      initialRouteName: 'Auth',
    }
  ));