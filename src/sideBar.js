import React, {Component} from 'react'
import {View, Text, StyleSheet, Linking} from 'react-native'
import {List, ListItem} from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigation } from 'react-navigation';

class SideBar extends Component {

    constructor (props) {
        super(props)
        this.state ={
          motorista_name: null,
          motorista_id: null
        
        }
      }
      componentDidMount(){
        AsyncStorage.getItem("display_name").then((value) => {
          this.setState({motorista_name: value})
        })
        AsyncStorage.getItem("id_motorista").then((value) => {
          this.setState({motorista_id: value})
        })
      }
      closed = () => {
        AsyncStorage.clear()
        this.props.navigation.navigate('Auth')
      }

      render () {
        //let cod = '15'
          return (
            <View style={styles.container}>
                <List>
            <ListItem itemHeader first>
              <Text>Olá, <Text style={{fontFamily:'Nunito', fontWeight:'bold'}}>{this.state.motorista_name}</Text> #{this.state.motorista_id}</Text>
            </ListItem>
            <ListItem button onPress={() => Linking.openURL('https://app.freteman.com.br/?entrar=true').catch((err) => console.error('An error occurred', err))}>
              <Text style={{fontFamily:'Nunito'}}>Minha conta</Text>
            </ListItem>
            <ListItem button onPress={() => Linking.openURL('whatsapp://send?text=Olá,%20sou%20motorista%20e%20preciso%20de%20suporte&phone=+5511996663819').catch((err) => console.error('An error occurred', err))}>
              <Text style={{fontFamily:'Nunito'}}>Whatsapp (Suporte)</Text>
            </ListItem>
            <ListItem button onPress={() => Linking.openURL('tel://011996663819').catch((err) => console.error('An error occurred', err))}>
              <Text style={{fontFamily:'Nunito'}}>Ligar (Suporte)</Text>
            </ListItem>
            <ListItem noBorder button onPress={()=> this.closed()}>
              <Text style={{fontFamily:'Nunito'}}>Sair do aplicativo</Text>
            </ListItem>
          </List>
            </View>
          )
      }

}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 20,
      backgroundColor: 'white',
    },
    controlText: {
      color: 'white',
    },
    button: {
      backgroundColor: 'white',
      borderWidth: 1,
      borderColor: 'black',
      padding: 10,
    }
  })
  export default withNavigation(SideBar);