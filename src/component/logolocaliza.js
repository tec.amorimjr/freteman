import React, {Component} from 'react';
import {Image} from 'react-native'

export default class Logolocaliza extends Component {
    render() {
      return (
        <Image
          source={require('../../image/image_localizacao.png')}
          style={{ width: 17, height: 18}}
        />
      );
    }
  }