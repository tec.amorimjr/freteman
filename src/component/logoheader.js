import React, {Component} from 'react';
import {Image} from 'react-native'

export default class LogoHeader extends Component {
    render() {
      return (
        <Image
          source={require('../../image/logo_header.png')}
          style={{ width: 160, height: 17 , marginLeft: 12}}
        />
      );
    }
  }