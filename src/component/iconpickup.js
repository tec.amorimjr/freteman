import React, {Component} from 'react';
import {Image} from 'react-native'

export default class IconPickup extends Component {
    render() {
      return (
        <Image
          source={require('../../image/pickup_location.png')}
        />
      );
    }
  }