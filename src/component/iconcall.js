import React, {Component} from 'react';
import {Image} from 'react-native'

export default class IconCall extends Component {
    render() {
      return (
        <Image
          source={require('../../image/icon_call.png')}
          style={{width:23, height:23}}
        />
      );
    }
  }