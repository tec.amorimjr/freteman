import React, {Component} from 'react';
import {Image} from 'react-native'

export default class IconDrop extends Component {
    render() {
      return (
        <Image
          source={require('../../image/drop_location.png')}
        />
      );
    }
  }