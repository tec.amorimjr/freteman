import React, {Component} from 'react';
import {Image} from 'react-native'

export default class LineDrop extends Component {
    render() {
      return (
        <Image
          source={require('../../image/Line_drop.png')}
          style={{height:130}}
        />
      );
    }
  }