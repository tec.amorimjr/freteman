import React, {Component} from 'react';
import {Image} from 'react-native'

export default class IconMap extends Component {
    render() {
      return (
        <Image
          source={require('../../image/ic_map.png')}
          style={{width:15, height:15}}
        />
      );
    }
  }